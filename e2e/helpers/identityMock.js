import faker from 'faker/locale/fr';
import RandExp from 'randexp';
import dayjs from 'dayjs';
import _ from 'lodash';

import db from './database';

export async function getMockData() {
    const [minAge, maxAge] = [16, 115];
    const dob = dayjs(faker.date.between(dayjs().subtract(maxAge, 'years'), dayjs().subtract(minAge, 'years')));

    const mock = {}
    mock.LastName = faker.name.lastName();
    mock.FirstName = faker.name.firstName();
    mock.Birthdate = dob.format('YYYYMMDD');
    mock.Gender = Math.floor(Math.random() * 2);
    mock.PostalCode = new RandExp(/[GHJ]\d[A-Z]\d[A-Z]\d/).gen();

    const hin = []
    hin.push(mock.LastName.replace(' ', '').slice(0, 3).toUpperCase());
    hin.push(mock.FirstName.slice(0, 1).toUpperCase());
    hin.push(dob.format('YYMMDD'));
    hin.push(new RandExp(/\d{2}/).gen());
    mock.HealthInsuranceNumber = hin.join("");

    mock.MotherMaidenName = faker.name.lastName();
    mock.MotherFirstName = faker.name.firstName();
    mock.FatherLastName = mock.LastName;
    mock.FatherFirstName = faker.name.firstName();

    mock.FirstVaccinationDate = dayjs(faker.date.past(1)).format('YYYYMMDD');
    const shotList = await db.queryFromFile('e2e/fixtures/CovidVaccineCodes.sql');
    mock.VaccineManufacturer = _.sample(shotList).HumanReadableName;

    // console.log(mock);
    return mock;
}