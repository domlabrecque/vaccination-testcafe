import { Pool } from 'pg';
import fs from 'fs'

const pgPool = new Pool({
    ssl: {
        rejectUnauthorized: false
    }
});


module.exports = {
    async queryFromString(query) {
        const result = await pgPool.query(query);
        return result.rows;
    },
    async queryFromFile(file) {
        const query = fs.readFileSync(file, {encoding: 'utf8', flag:'r'});
        return await this.queryFromString(query)
    },
}