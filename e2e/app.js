//app.js
require('dotenv').config();
import config from './config';
import minimist from 'minimist';

class App {
    constructor() {
        this.initialized = false;
        this.activeFixturesCount = 0;
        let args = minimist(process.argv.slice(2));
        this.env = args.env || 'dev'
        this.db = args.db || this.env;
        this.baseUrl = config[this.env].baseUrl;
        
        // Priority: PG > ENV_PG. Can be set with --env or --db
        const { PGHOST, PGDATABASE, PGUSER, PGPASSWORD, PGPORT } = process.env;
        this.db = this.db.toUpperCase();
        process.env.PGHOST =        PGHOST      || process.env[`${this.db}_PGHOST`];
        process.env.PGDATABASE =    PGDATABASE  || process.env[`${this.db}_PGDATABASE`];
        process.env.PGUSER =        PGUSER      || process.env[`${this.db}_PGUSER`];
        process.env.PGPASSWORD =    PGPASSWORD  || process.env[`${this.db}_PGPASSWORD`];
        process.env.PGPORT =        PGPORT      || process.env[`${this.db}_PGPORT`];

        // assign other variables
    }

    async before() {
        if (!this.initialized) {
            // setup
            this.initialized = true;
        }
        this.activeFixturesCount++;
    }

    async after() {
        this.activeFixturesCount--;
        if (!this.activeFixturesCount) {
            // teardown
        }
    }
}

export default new App()