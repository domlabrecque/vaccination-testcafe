import { t, Selector, ClientFunction } from 'testcafe';
const pdf = require('pdf-parse');

const getInputByName = (name) => `input[name="${name}"]`;
const cssInvalid = 'div.invalid-message';

class Page {
    constructor() {
        this.title = Selector('h1.theme-titre');
        this.button_language = Selector('button.btn-link').withText(/(english|fran.ais)/i);

        // Identity
        this.input_lastName = Selector(getInputByName('lastName'));
        this.invalid_lastName = Selector(getInputByName('lastName')).sibling(cssInvalid);
        this.input_firstName = Selector(getInputByName('firstName'));
        this.invalid_firstName = Selector(getInputByName('firstName')).sibling(cssInvalid);;
        this.input_birthDate = Selector(getInputByName('birthDate'));
        this.invalid_birthDate = Selector(getInputByName('birthDate'))
            .parent('div.react-datepicker-wrapper')
            .sibling(cssInvalid);
        this.input_postalCode = Selector(getInputByName('postalCode'));
        this.invalid_postalCode = Selector(getInputByName('postalCode')).sibling(cssInvalid);
        this.input_HIN = Selector(getInputByName('healthInsuranceNumber'));
        this.invalid_HIN = Selector(getInputByName('healthInsuranceNumber')).sibling(cssInvalid);
        this.box_unknownHIN = Selector(getInputByName('isHealthInsuranceNumberUnknown'));

        // Mother / Father
        this.input_motherMaidenName = Selector(getInputByName('motherMaidenName'));
        this.input_motherFirstName = Selector(getInputByName('motherFirstName'));
        this.box_isMotherUnknown = Selector(getInputByName('isMotherUnknown'));
        this.input_fatherLastName = Selector(getInputByName('fatherLastName'));
        this.input_fatherFirstName = Selector(getInputByName('fatherFirstName'));
        this.box_isFatherUnknown = Selector(getInputByName('isFatherUnknown'));
        this.invalid_motherMaidenName = Selector(getInputByName('motherMaidenName')).sibling(cssInvalid);
        this.invalid_motherFirstName = Selector(getInputByName('motherFirstName')).sibling(cssInvalid);

        // Vaccination Info
        this.input_firstVaxDate = Selector(getInputByName('firstVaccinationDate'));
        this.invalid_firstVaxDate = Selector(getInputByName('firstVaccinationDate'))
            .parent('div.react-datepicker-wrapper')
            .sibling(cssInvalid);
        this.vaxManufacturer = {
            container: 'div[class$="-container"]',
            input: Selector('input[id^="react-select-"][id$="-input"]'),
            options: Selector('div[id^="react-select-"][id*="-option-"]')
        };

        // Submit
        this.box_consent = Selector(getInputByName('consent'));
        this.button_submit = Selector('button[type="submit"]');

        // Warning modal
        this.modal = Selector('div.modal-dialog[title="Warning"]');

        // Download page
        this.button_download = Selector('button[data-test="download_vaccination_proof"]');
        this.button_download_card = Selector('button[data-test="download_vaccination_proof_card"]');
    }

    async getPageLanguage() {
        return await Selector('html').getAttribute('lang');
    }

    async setPageLanguage(lang = undefined) {
        if (lang !== this.language) {
            await t.click(this.button_language);
        }
    }

    async setPageLanguageRandom() {
        if (Math.floor(Math.random() * 2)) {
            this.setPageLanguage();
        }
    }

    selectGender(gender) {
        return Selector(`input[name^="gender"][value="${gender}"]`);
    }

    async fillForm(data) {
        await this.fillFormPart1(data);
        await this.fillFormPart2(data);
        await this.fillFormPart3(data);
        await this.clickConsent();
    }

    async fillFormPart1(data) {
        await t
            .typeText(this.input_lastName, data.LastName)
            .typeText(this.input_firstName, data.FirstName)
            .typeText(this.input_birthDate, data.Birthdate);
        if (data.PostalCode) {
            await t.typeText(this.input_postalCode, data.PostalCode);
        }
        await t.click(this.selectGender(data.Gender));
        if (data.HealthInsuranceNumber) {
            await t.typeText(this.input_HIN, data.HealthInsuranceNumber);
        } else {
            await t.click(this.box_unknownHIN);
        }
    }

    async fillFormPart2(data) {
        if (data.MotherMaidenName) {
            await t
                .typeText(this.input_motherMaidenName, data.MotherMaidenName)
                .typeText(this.input_motherFirstName, data.MotherFirstName);
        } else {
            await t.click(this.box_isMotherUnknown);
        }
        if (data.FatherLastName) {
            await t
                .typeText(this.input_fatherLastName, data.FatherLastName)
                .typeText(this.input_fatherFirstName, data.FatherFirstName);
        } else {
            await t.click(this.box_isFatherUnknown);
        }
    }

    async fillFormPart3(data) {
        await t.typeText(this.input_firstVaxDate, data.FirstVaccinationDate);

        const container = this.vaxManufacturer.input.parent(this.vaxManufacturer.container);
        const option = this.vaxManufacturer.options.withExactText(data.VaccineManufacturer);

        await t.click(container).click(option);

    }

    async clickConsent() {
        await t.click(this.box_consent);
    }

    async clickSubmitForm() {
        await t.click(this.button_submit);
    }

    async submitFormSuccess() {
        const getLocationPart = ClientFunction(locationPart => {
            return window.location[locationPart];
        });
        await this.clickSubmitForm();
        await t
            .expect(getLocationPart('href'))
            .contains('token=', { timeout: 10000 });
    }

    async submitFormWarningPopup() {
        await t
            .click(this.button_submit)
            .expect(this.modal.textContent).match(/(warning|attention)/i, { timeout: 20000 })
    }

    async downloadVaccinationProof(data, reqLogger) {
        await t
            .click(this.button_download)
            .expect(reqLogger.contains(r => r.response.statusCode === 200)).ok({ timeout: 30000 })
            .expect(reqLogger.contains(r => r.response.headers['content-type'].endsWith('pdf'))).ok();
        const buffer = reqLogger.requests[0].response.body;
        const pdfData = await pdf(buffer);
        await t
            .expect(pdfData.text).contains(data.FirstName)
            .expect(pdfData.text).contains(data.LastName);
    }
}

export default new Page();