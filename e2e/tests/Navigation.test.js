import { Selector } from 'testcafe';

import app from '../app';
import formPage from '../page_model/IdentificationForm.page';

fixture`Navigation`
.before(async ctx => {
    app.before();
})
.after(async ctx => {
    await app.after();
});

test('Url /fr displays form in French html tag has attribute lang=fr', async t => {
    await t
        .navigateTo(`${app.baseUrl}/fr`)
        .expect(formPage.title.textContent).eql('Preuve de vaccination électronique')
        .expect(formPage.button_submit.textContent).eql('SOUMETTRE')
        .expect(Selector('html').withAttribute('lang', 'fr').exists).ok();
});


test('Url /en displays form in English html tag has attribute lang=en', async t => {
    await t
    .navigateTo(`${app.baseUrl}/en`)
        .expect(formPage.title.textContent).eql('Electronic Proof of Vaccination')
        .expect(formPage.button_submit.textContent).eql('SUBMIT')
        .expect(Selector('html').withAttribute('lang', 'en').exists).ok()
});


// since v1.18, /maintenance redirects to /PreuveVaccinale automatically
test.skip('Url /maintenance affiche la page de maintenance', async t => {
    await t
    .navigateTo(`${app.baseUrl}/maintenance`)
        .expect(formPage.title.textContent).eql('Maintenance');
});