import { RequestLogger } from 'testcafe';
import _ from 'lodash';

import app from '../app';
import db from '../helpers/database';
import formPage from '../page_model/IdentificationForm.page';

const logger = RequestLogger({ url: /akivacc.+storage\.blob/, method: 'GET' }, {
    logResponseHeaders: true,
    logResponseBody: true
});



fixture.skip`Download Proof as PDF`
    .page(app.baseUrl)
    .before(async ctx => {
        app.before();
    })
    .after(async ctx => {
        await app.after();
    })
    .beforeEach(async t => {
        await formPage.setPageLanguageRandom();
    })
    .requestHooks(logger);


test(`Patient with valid data is able to download vaccination proof`, async t => {
    const dataList = await db.queryFromFile('e2e/fixtures/MedicalPatients-AllFields.sql');
    const data = _.sample(dataList);
    await formPage.fillForm(data);
    await formPage.submitFormSuccess();
    await formPage.downloadVaccinationProof(data, logger);
});