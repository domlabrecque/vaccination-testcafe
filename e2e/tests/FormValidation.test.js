import dayjs from 'dayjs';
import { RequestLogger } from 'testcafe';

import app from '../app';
import formPage from '../page_model/IdentificationForm.page';
import { getMockData } from '../helpers/identityMock';

const candidatesGetLogger = RequestLogger({ url: /api\/v1\.3\/candidates\/get/, method: 'POST' }, {
    logResponseHeaders: true
});

fixture`Form Validation`
    .page(app.baseUrl)
    .before(async ctx => {
        app.before();
    })
    .after(async ctx => {
        await app.after();
    })
    .beforeEach(async t => {
        await formPage.setPageLanguageRandom();
        t.ctx.lang = await formPage.getPageLanguage();
    })
    .requestHooks(candidatesGetLogger);


test('Postal code required if age < 14', async t => {
    const warning = {
        fr: "* Vous devez fournir un code postal si vous êtes âgé de moins de 14 ans",
        en: "* You must provide a postal code if you are less than 14 years old"
    };
    const data = await getMockData();
    
    // set dob to 13 today, 14 tomorrow
    data.Birthdate = dayjs().subtract(14, 'years').add(1, 'day').format('YYYYMMDD');
    // empty postalcode and save original
    data.PostalCodeBackup = data.PostalCode;
    data.PostalCode = '';

    await formPage.fillForm(data);
    await formPage.clickSubmitForm();
    await t
        // clicking submit should not query backend
        .expect(candidatesGetLogger.count(() => true)).eql(0)
        
        // invalid message displayed
        .expect(formPage.invalid_postalCode.visible).ok()
        .expect(formPage.invalid_postalCode.textContent).eql(warning[t.ctx.lang])

        // enter original postalcode -> invalid message not displayed
        .typeText(formPage.input_postalCode, data.PostalCodeBackup)
        .expect(formPage.invalid_postalCode.exists).notOk();
});


test("Can't submit form if age < 12", async t => {
    const warning = {
        fr: "* Vous devez avoir au moins 12 ans",
        en: "* You must be at least 12 years of age"
    };
    const data = await getMockData();

    // set dob to 11 today, 12 tomorrow
    data.Birthdate = dayjs().subtract(12, 'years').add(1, 'day').format('YYYYMMDD');
    await formPage.fillForm(data);
    await formPage.clickSubmitForm();
    await t
        // clicking submit should not query backend
        .expect(candidatesGetLogger.count(() => true)).eql(0)

        // invalid message displayed
        .expect(formPage.invalid_birthDate.visible).ok()
        .expect(formPage.invalid_birthDate.textContent).eql(warning[t.ctx.lang])
});


test("Must provide Mother name if HIN unknown", async t => {
    const warning = {
        first : {
            en: '* Mother first name is required',
            fr: '* Prénom de la mère requis'
        },
        maiden: {
            en: '* Mother maiden name is required',
            fr: '* Nom de la mère requis'
        },
        hin: {
            en: '* Health insurance number is required',
            fr: "* Numéro d'assurance maladie requis"
        }
    }
    const data = await getMockData();
    data.HealthInsuranceNumber = undefined;
    data.MotherFirstName = undefined;
    data.MotherMaidenName = undefined;
    await formPage.fillForm(data);
    await formPage.clickSubmitForm();
    await t
        // clicking submit should not query backend
        .expect(candidatesGetLogger.count(() => true)).eql(0)
        
        // invalid message displayed for mother name
        .expect(formPage.invalid_motherFirstName.visible).ok()
        .expect(formPage.invalid_motherFirstName.textContent).eql(warning.first[t.ctx.lang])
        .expect(formPage.invalid_motherMaidenName.visible).ok()
        .expect(formPage.invalid_motherMaidenName.textContent).eql(warning.maiden[t.ctx.lang])

        // hin known -> hin invalid message displayed
        .click(formPage.box_unknownHIN)
        .expect(formPage.invalid_HIN.visible).ok()
        .expect(formPage.invalid_HIN.textContent).eql(warning.hin[t.ctx.lang])
        
        // mother name invalide not displayed
        .expect(formPage.invalid_motherFirstName.exists).notOk()
        .expect(formPage.invalid_motherMaidenName.exists).notOk();

});