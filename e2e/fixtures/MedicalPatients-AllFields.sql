SELECT 
	mp."LastName", 
	mp."FirstName",
	to_char(mp."Birthdate", 'YYYYMMDD') as "Birthdate",
	mp."Gender",
	mp."HealthInsuranceNumber",
	mp."PostalCode",
	mp."MotherMaidenName",
	mp."MotherFirstName",
	mp."FatherLastName",
	mp."FatherFirstName",
	to_char(mi."Date", 'YYYYMMDD') as "FirstVaccinationDate",
	cvc."HumanReadableName" AS "VaccineManufacturer"
FROM public."MedicalPatients" 			AS mp
	JOIN public."MedicalImmunizations" 	AS mi 	ON mp."Id" = mi."MedicalPatientId"
	JOIN public."CovidVaccineCodes" 	AS cvc 	ON mi."TradeName" = cvc."TradeName"
WHERE mp."HealthInsuranceNumber" notnull 
	AND mp."PostalCode" notnull  
	AND mp."MotherFirstName" <> ''
	AND mp."FatherFirstName" <> ''