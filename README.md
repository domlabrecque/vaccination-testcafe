# Tests automatisés avec Testcafe et Browserstack #

1. [Structure du projet](#structure-du-projet)  
2. [Configuration](#configuration)    
3. [Exécuter un test](#executer-un-test)    
4. [Ajouter un test](#ajouter-un-test)  
5. [Teamcity setup](#teamcity-setup)  
6. [Browserstack setup](#browserstack-setup)

---

## Structure du projet
* __.testcaferc.json__ : Configuration locale de testcafe
* __teamcity.testcaferc.json__ : Configuration spécifique à Teamcity
* __src__
    * __fixtures__ : Données de test
    * [__page_model__](https://testcafe.io/documentation/402826/guides/concepts/page-model) : Sélecteurs et logiques par pages   
    * __tests__ : Fichiers de tests

## Configuration
Reference:

* [Fichier de configuration _.testcaferc.json_](https://testcafe.io/documentation/402638/reference/configuration-file)
* [Arguments passés en CLI](https://testcafe.io/documentation/402639/reference/command-line-interface)

Testcafe lit d'abord le fichier de configuration et ensuite les paramètres passés en CLI. Donc les arguments passés en CLI override ceux du fichier de configuration _.testcaferc.json_.

---

## Exécuter un test
[Guide rapide officiel de testcafe.io](https://testcafe.io/documentation/402635/getting-started#running-the-test)

Avec __yarn__:
```
yarn testcafe <browser(s)> <src> [arguments]
```
Avec __npm__:
```
npx testcafe <browser(s)> <src> [arguments]
```
Si __\<browser(s)\>__ et __\<src\>__ sont spécifiés dans le fichier de config _.testcaferc.json_, ils peuvent être omis. Sinon, ils sont obligatoires.

[__browser(s)__](https://testcafe.io/documentation/402639/reference/command-line-interface#browser-list) => alias de browsers à utiliser pour tester.  
Pour afficher une liste des alias des browsers disponibles localement:
```
npx testcafe --list-browsers
```

[__src__](https://testcafe.io/documentation/402639/reference/command-line-interface#file-pathglob-pattern) => fichier de tests.

### Examples ###
1 browser, un fichier de tests:
```
npx testcafe chrome src/tests/vaxProof.test.js
```

Plusieurs browsers et src en format _glob_:
```
npx testcafe firefox,chrome src/**/*.test.js
```
Browser et src déjà configurés dans _.testcaferc.json_:
```
npx testcafe
```

---

## Ajouter un test
Dans le dossier _root/src/test/_, sélectionner un fichier _*.test.js_ ou en créer un nouveau.

... À compléter

---

## Teamcity setup
Projet : [Vaccination - Tests automatisés](https://akinox-teamcity.azurefd.net/project.html?projectId=VaccinationTestsAutomatises&tab=projectOverview)  

Sous-projet : [Vaccination Testcafe](https://akinox-teamcity.azurefd.net/project.html?projectId=VaccinationTestsAutomatises_VaccinationTestcafe&tab=projectOverview)  
Contient les variables d'environnements pour l'accès à Browserstack.

Builds:

* Composite : [Vaccination Testcafe Composite](https://akinox-teamcity.azurefd.net/viewType.html?buildTypeId=VaccinationTestsAutomatises_VaccinationTestcafe_VaccinationTestcafeComposite)  
Le composite sert à regrouper plusieurs builds combinés par _snapshot dependencies_ et les présenter à un seul endroit. En d'autres mots, c'est un build composé de plusieurs parties pouvant être exécutées en parallèle sur plusieurs agents.

* Template : _testcafe docker run template_  
Ce template est utilisé comme base pour les builds attachés au composite  
Il contient 1 étape:
    1. node.js avec docker node:alpine  

* Builds attachés au composite et basés sur le template:
    * Windows-Chrome
    * Windows-Firefox
    * Windows-Edge
    * MacOSX-Safari

Pour lancer les tests sur toutes les plateformes ci-haut, exécuter le composite. Le composite lancera tous les builds qui lui sont attachés.

Autrement, pour lancer les tests sur un browser spécifique, lancer le build du browser voulu.

---

## Browserstack setup
Browserstack offre une période d'essai incluant 100 minutes d'automatisation pour ceux qui veulent l'essayer.

Browserstack pour testcafe nécessite le package [testcafe-browser-provider-browserstack](https://github.com/DevExpress/testcafe-browser-provider-browserstack)

Pour utiliser Browserstack localement, vous devez sauvegarder votre username et access key dans des variables d'environnement.
```
BROWSERSTACK_USERNAME="******"
BROWSERSTACK_ACCESS_KEY="**********"
```
Dans teamcity, ces variables sont configurées dans le sous-projet Vaccination Testcafe


Pour vérifier que votre accès fonctionne, affichez une liste des browsers disponibles sur Browserstack:
```
npx testcafe --list-browsers browserstack
```
Pour exécuter un test sur Browserstack, remplacer simplement l'alias du browser par un alias de Browserstack:
```
npx testcafe "browserstack:chrome:OS X" <src>
```